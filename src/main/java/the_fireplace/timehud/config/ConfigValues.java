package the_fireplace.timehud.config;

/**
 * @author The_Fireplace
 */
public class ConfigValues {
	public static final String LOCATION_DEFAULT = "top-left";
	public static String LOCATION;
	public static final String LOCATION_NAME = "cfg.location";

	public static final String FORMAT_DEFAULT = "24HH:MMBRNAME DATE, YEAR";
	public static String FORMAT;
	public static final String FORMAT_NAME = "cfg.format";

	public static final boolean REAL_DEFAULT = false;
	public static boolean REAL;
	public static final String REAL_NAME = "cfg.real";

	public static final boolean NEEDCLOCK_DEFAULT = false;
	public static boolean NEEDCLOCK;
	public static final String NEEDCLOCK_NAME = "cfg.needclock";
}
